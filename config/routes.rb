Rails.application.routes.draw do
  
  resources :events
  resources :rsvp
  get 'users/show'
  get 'meetup_groups/members'
  devise_for :users
  resources :meetup_groups do
    resources :joins
    resources :comments
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  resources :categories
  get 'categories/new'
 
end
