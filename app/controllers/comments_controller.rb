class CommentsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  def create
    @meetup_group = MeetupGroup.find(params[:meetup_group_id])
    @comment = @meetup_group.comments.create(comment_params)

    redirect_to meetup_group_path(@meetup_group)
  end

   private
    # Use callbacks to share common setup or constraints between actions.
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      params.require(:comment).permit(:user_id, :body)
    end

end
