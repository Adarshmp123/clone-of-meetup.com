class HomeController < ApplicationController
  def index

    if current_user          
      @meetup_groups = MeetupGroup.all
    else  
      @categories = Category.all
    end
  end

  

end
