class UsersController < ApplicationController
  def show
    @meetups = MeetupGroup.where(user_id: params[:id]) 
    @meetupcount = @meetups.count
    @joins = Join.where(user_id: params[:id])
    @joincount = @joins.count
    @user = User.find(params[:id])
  end
  
  
end
