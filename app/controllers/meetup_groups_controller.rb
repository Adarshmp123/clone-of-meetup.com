class MeetupGroupsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show, :members]
  before_action :set_meetup_group, only: [:show, :edit, :update, :destroy]

  # GET /joins
  # GET /joins.json
  def index
    if user_signed_in?
      @meetup_groups = MeetupGroup.where(category_id: params[:id])
    else
      @meetup_groups = MeetupGroup.all
    end
  end

  # GET /joins/1
  # GET /joins/1.json
  def show
    if user_signed_in?
      @joins = Join.where(user_id: current_user.id, meetup_group_id: @meetup_group.id)
     end
      @meetup_groups = MeetupGroup.where(category_id: params[:id])    
      @events = Event.where(meetup_group_id: @meetup_group.id)
      @members = Join.where(meetup_group_id: @meetup_group.id)
   
  end

  def members
    @users = Join.where(meetup_group_id: params[:id])
  end


  # GET /joins/new
  def new
    if user_signed_in?
      @meetup_group = MeetupGroup.new
    end
  end

  # GET /joins/1/edit
  def edit
  end

  # POST /joins
  # POST /joins.json
  def create
    @meetup_group = MeetupGroup.new(meetup_group_params)

    respond_to do |format|
      if @meetup_group.save
        @join = Join.new(:user_id => current_user.id, :meetup_group_id => @meetup_group.id)
        @join.save
        format.html { redirect_to @meetup_group, notice: 'Join was successfully created.' }
        format.json { render :show, status: :created, location: @meetup_group }
      else
        format.html { render :new }
        format.json { render json: @meetup_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /joins/1
  # PATCH/PUT /joins/1.json
  def update
    respond_to do |format|
      if @meetup_group.update(meetup_group_params)
        format.html { redirect_to @meetup_group, notice: 'Join was successfully updated.' }
        format.json { render :show, status: :ok, location: @meetup_group }
      else
        format.html { render :edit }
        format.json { render json: @meetup_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /joins/1
  # DELETE /joins/1.json
  def destroy
    @meetup_group.destroy
    respond_to do |format|
      format.html { redirect_to meetup_groups_url, notice: 'Join was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_meetup_group
      @meetup_group = MeetupGroup.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def meetup_group_params
      params.require(:meetup_group).permit(:name, :location, :latitude, :longitude, :category_id, :about, :user_id)
    end




end

