class Event < ApplicationRecord
  belongs_to :meetup_group
  has_many :rsvp
  geocoded_by :location
  after_validation :geocode 

  validates :name, presence: true
  validates :event_sub, presence: true
  validates :start_date, presence: true
  validates :about, presence: true  
end