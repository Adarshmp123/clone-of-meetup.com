class MeetupGroup < ApplicationRecord
  has_many :comments
  has_many :joins
  has_many :events
  geocoded_by :location
  after_validation :geocode 

  validates :name, presence: true  
  validates :category_id, presence: true  
  validates :about, presence: true  
end