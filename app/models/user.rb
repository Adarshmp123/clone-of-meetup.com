class User < ApplicationRecord

  mount_uploader :avatar, AvatarUploader
  geocoded_by :location
  after_validation :geocode 
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

   # User Avatar Validation
  validates_integrity_of  :avatar
  validates_processing_of :avatar

end
