class Comment < ApplicationRecord
  belongs_to :meetup_group
  validates :body, presence: true
end
