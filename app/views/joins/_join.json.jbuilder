json.extract! join, :id, :user_id, :meetup_group_id, :created_at, :updated_at
json.url join_url(join, format: :json)