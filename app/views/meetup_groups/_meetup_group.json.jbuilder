json.extract! meetup_group, :id, :name, :location, :latitude, :longitude, :category_id, :about, :created_at, :updated_at
json.url meetup_group_url(meetup_group, format: :json)