class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|

      t.string :name
      t.string :location
      t.text :about
      t.date :start_date
      t.date :end_date
      t.string :price
      t.string :total_seats
      t.string :event_sub

      t.timestamps
    end
  end
end
