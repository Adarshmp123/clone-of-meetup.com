class CreateMeetupGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :meetup_groups do |t|
      t.string :name
      t.string :location
      t.float :latitude
      t.float :longitude
      t.integer :category_id
      t.string :about
      t.integer :user_id

      t.timestamps
    end
  end
end
